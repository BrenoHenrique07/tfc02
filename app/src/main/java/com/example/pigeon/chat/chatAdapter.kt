package com.example.pigeon.chat

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Adapter
import com.example.pigeon.R

class chatAdapter (
    private var chats: ArrayList<Chat>
 ) : androidx.recyclerview.widget.RecyclerView.Adapter<chatViewHolder>() {

     fun interface onClickChatListener {
         fun onClick(chat: Chat)
     }

     private  var listener: onClickChatListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): chatViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(
            R.layout.item_view_chat, parent, false
        )
        return chatViewHolder(itemView, this)
    }

    override fun onBindViewHolder(holder: chatViewHolder, position: Int) {
        val chat = this.chats[position]
        holder.bind(chat)
    }

    override fun getItemCount(): Int {
        return this.chats.size
    }

    fun setOnClickChannelListener(listener: onClickChatListener?) {
        this.listener = listener
    }

    fun getOnClickTaskListener(): onClickChatListener? {
        return this.listener
    }
}