package com.example.pigeon.chat

import com.example.pigeon.message.Message

open class Chat (
    val name: String,
    var id: Long = 0
) {
    constructor(id: Long): this("") {
        this.id = id
    }

    private val msnArray = ArrayList<Message>()

    fun addMs(message: Message){
        this.msnArray.add(message)
    }

    fun getMs() : ArrayList<Message>{
        return this.msnArray
    }


}