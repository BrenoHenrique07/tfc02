package com.example.pigeon.chat

object DAOChatSingleton {
    private var serial: Long = 1
    private val chats = ArrayList<Chat>()

    fun add() {
        val chat = Chat("Grupo $serial")
        this.chats.add(0, chat)

        chat.id = serial++
    }

    fun getChats(): ArrayList<Chat> {
        return this.chats
    }

    fun getChatById(id: Long): Chat? {
        for(c in this.chats) {
            if(c.id == id)
                return c
        }
        return null
    }

    fun getChatPositionById(id: Long): Int {
        return this.chats.indexOf(Chat(id))
    }
}