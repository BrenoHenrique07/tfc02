package com.example.pigeon.chat

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.pigeon.R

class chatViewHolder (
    itemView: View,
    protected val adapter: chatAdapter
) : ViewHolder (itemView) {
        private val txtChatName = itemView.findViewById<TextView>(R.id.txtNameChat)
        private val txtLastMsg = itemView.findViewById<TextView>(R.id.txtLastMsg)
        protected lateinit var currentChat: Chat

        init {
            this.itemView.setOnClickListener {
                this.adapter
                    .getOnClickTaskListener()?.onClick(this.currentChat)
            }
        }

        open fun bind (chat: Chat) {
            this.currentChat = chat
            this.txtChatName.text = this.currentChat.name
            this.txtLastMsg.text = "..."
        }
    }
