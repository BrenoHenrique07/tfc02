package com.example.pigeon

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pigeon.chat.Chat
import com.example.pigeon.chat.DAOChatSingleton
import com.example.pigeon.chat.chatAdapter
import com.example.pigeon.message.DAOMsnSingleton
import com.example.pigeon.message.Message
import com.example.pigeon.message.MsnAdapter
import com.example.pigeon.message.MsnViewHolder
import kotlin.random.Random

class ChatActivity : AppCompatActivity() {

    private lateinit var rView2: RecyclerView
    private lateinit var btnSendMsn: Button
    private lateinit var extMsn: EditText
    private var serial: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        this.rView2 = findViewById(R.id.recyclerView2)
        this.btnSendMsn = findViewById(R.id.button2)
        this.extMsn = findViewById(R.id.editTextTextPersonName)

        val chatID = intent.getLongExtra("chatID", -1)
        val chatT = DAOChatSingleton.getChatById(chatID)
        this.rView2.layoutManager = LinearLayoutManager(this)
        if(chatT != null) {
            this.rView2.adapter = MsnAdapter(chatT.getMs())
        }
    }

    fun onClickInsert(view: View) {
        val txt = this.extMsn.text.toString()
        if (!txt.isBlank()) {
            val ms = Message(serial++, txt, name())
            val chatID = intent.getLongExtra("chatID", -1)
            val chatT = DAOChatSingleton.getChatById(chatID)

            if(chatT != null) {
                chatT.addMs(ms)
                this.rView2.adapter?.notifyItemInserted(chatT.getMs().size-1)
                this.rView2.smoothScrollToPosition(chatT.getMs().size-1)
            }
        }
    }

    private fun name(): String{
        if(Random.nextBoolean()) {
            return "Ciclano"
        } else {
            return "Fulano"
        }
    }
}