package com.example.pigeon

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.size
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pigeon.chat.DAOChatSingleton
import com.example.pigeon.chat.chatAdapter

class MainActivity : AppCompatActivity() {

    private lateinit var rView: RecyclerView
    private lateinit var btnCreateChat: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.rView = findViewById(R.id.rView)
        this.btnCreateChat = findViewById(R.id.button)
        this.rView.layoutManager = LinearLayoutManager(this)

        val adapter = chatAdapter(DAOChatSingleton.getChats())
        this.rView.adapter = adapter
        val result = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            if(it.resultCode == RESULT_OK && it.data != null) {
                val chatID = it.data!!.getLongExtra("chatID", -1)
                val chatP = DAOChatSingleton.getChatPositionById(chatID)
                adapter.notifyItemChanged(chatP)
            }
        }
        adapter.setOnClickChannelListener {
            val intent =
                Intent(this, ChatActivity::class.java)
            intent.putExtra("chatID", it.id)
            result.launch(intent)
        }
    }

    fun onClick (view: View) {
        DAOChatSingleton.add()
        this.rView.adapter?.notifyItemInserted(0)
        this.rView.smoothScrollToPosition(0)
    }
}