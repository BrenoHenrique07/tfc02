package com.example.pigeon.message

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.pigeon.R
import kotlin.random.Random

class MsnViewHolder (
    itemView: View,
    protected val adapter: MsnAdapter
): ViewHolder(itemView) {
    private val msnName = itemView.findViewById<TextView>(R.id.textView3)
    private val msnText = itemView.findViewById<TextView>(R.id.textView6)
    private val msnHour = itemView.findViewById<TextView>(R.id.textView7)

    protected lateinit var currentMsn: Message

    open fun bind(message: Message) {

        this.currentMsn = message
        this.msnName.text = this.currentMsn.person
        this.msnText.text = this.currentMsn.text
        this.msnHour.text = this.currentMsn.format()
    }
}