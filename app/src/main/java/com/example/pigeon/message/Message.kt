package com.example.pigeon.message

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class Message (
    var sent: Boolean,
    var person: String,
    var text: String,
    val hour: LocalDateTime,
    var id: Long
) {

    constructor(id: Long, text: String, person2: String) : this(true,"", "", LocalDateTime.now(), 0) {
        this.id = id
        this.text = text
        this.person = person2
    }

    fun format(): String {
        return  this.hour.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
    }
}