package com.example.pigeon.message

import com.example.pigeon.chat.Chat

object DAOMsnSingleton {
    private var serial: Long = 1
    private val msns2 = ArrayList<Message>()

    fun add(m: Message) {
        msns2.add(m)

        m.id = serial++
    }

    fun getChats(): ArrayList<Message> {
        return this.msns2
    }

    fun getChatPositionById(id: Long): Int {
        return this.msns2.indexOf(Message(id, "", ""))
    }
}