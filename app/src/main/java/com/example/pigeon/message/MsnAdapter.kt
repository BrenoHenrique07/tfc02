package com.example.pigeon.message

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.pigeon.R
import kotlin.random.Random

class MsnAdapter (
    private var msns: ArrayList<Message>
): Adapter<MsnViewHolder>() {

    private var flag: Boolean = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MsnViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(
            if(flag == true){
                flag = false
                R.layout.item_view_balloon_me
            } else {
                flag = true
                R.layout.item_view_balloon_you
            }
            , parent, false)
        return MsnViewHolder(itemView, this)
    }

    override fun onBindViewHolder(holder: MsnViewHolder, position: Int) {
        val message = this.msns[position]
        holder.bind(message)
    }

    override fun getItemCount(): Int {
        return this.msns.size
    }
}

